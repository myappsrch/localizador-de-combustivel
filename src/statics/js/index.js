import notificacoes from '@/services/commons/notificacoes.js'
import firebase from 'firebase'

var config = {
    apiKey: "AIzaSyBqwtioZjKI1Fk_-e1CcXtSqjWxk8uJwv0",
    authDomain: "localiza-combustivel.firebaseapp.com",
    databaseURL: "https://localiza-combustivel.firebaseio.com",
    projectId: "localiza-combustivel",
    storageBucket: "localiza-combustivel.appspot.com",
    messagingSenderId: "118555909336",
    appId: "1:118555909336:web:9976665f0df79e609dcbad"
}
firebase.initializeApp(config)

var usersRef = firebase.database().ref('users')

export default {
    mounted: function() {},
    data() {
        return {
            novoUsuario: {
                username: null,
                senha: null
            },
        }
    },

    firebase: {
        users: usersRef
    },

    watch: {

    },
    methods: {
        cadastrar() {
            usersRef.push(this.novoUsuario)
            this.novoUsuario.username = ''
            this.novoUsuario.senha = ''
        },

    }
}