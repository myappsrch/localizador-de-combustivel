import notificacoes from '@/services/commons/notificacoes.js'
import firebase from 'firebase'

var config = {
    apiKey: "AIzaSyBqwtioZjKI1Fk_-e1CcXtSqjWxk8uJwv0",
    authDomain: "localiza-combustivel.firebaseapp.com",
    databaseURL: "https://localiza-combustivel.firebaseio.com",
    projectId: "localiza-combustivel",
    storageBucket: "localiza-combustivel.appspot.com",
    messagingSenderId: "118555909336",
    appId: "1:118555909336:web:9976665f0df79e609dcbad"
}
firebase.initializeApp(config)

var posto = firebase.database().ref('posto')

export default {
    mounted: function() {

    },
    data() {
        return {
            loading: false,
            form: {
                nome: null,
                cnpj: null,
                regiao: null,
                companhia: null
            },
            optionsCompanhia: [
                'Texaco', 'Petrobras', 'ALE', 'Ipiranga'
            ],
            optionsRegiao: [
                'Texaco', 'Petrobras', 'ALE', 'Ipiranga'
            ]

        }
    },

    firebase: {
        users: posto
    },

    watch: {

    },
    methods: {
        onReset() {
            for (let form in this.form) {
                this.form[form] = null;
            }
        },
        cadastrar() {
            usersRef.push(this.form)
        },

    }
}