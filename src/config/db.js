import { firebase } from '@firebase/app'
import "@firebase/firestore";

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyBqwtioZjKI1Fk_-e1CcXtSqjWxk8uJwv0",
    authDomain: "localiza-combustivel.firebaseapp.com",
    databaseURL: "https://localiza-combustivel.firebaseio.com",
    projectId: "localiza-combustivel",
    storageBucket: "localiza-combustivel.appspot.com",
    messagingSenderId: "118555909336",
    appId: "1:118555909336:web:9976665f0df79e609dcbad"
});
export const db = firebaseApp.firestore();