import Vue from 'vue'
import Router from 'vue-router'
import DefaultLayout from './layouts/Default.vue'
import LoginLayout from './layouts/LoginTemplate.vue'
import login from '@/views/Login.vue'

let registrar = {
    path: '/registrar',
    component: () =>
        import ('@/views/SingUp.vue'),
    children: [{
        path: '',
        component: () =>
            import ('@/views/SingUp.vue')
    }]
}

let home = {
    path: '/home',
    component: () =>
        import ('@/views/Home.vue'),
    children: [{
        path: '',
        component: () =>
            import ('@/views/Home.vue')
    }]
}

let companhia = {
    path: '/companhia',
    component: () =>
        import ('@/views/cadastro/Companhia.vue'),
    children: [{
        path: '',
        component: () =>
            import ('@/views/cadastro/Companhia.vue')
    }],
    meta: {
        requeresAuth: true
    }
}

let produto = {
    path: '/produto',
    component: () =>
        import ('@/views/cadastro/Produto.vue'),
    children: [{
        path: '',
        component: () =>
            import ('@/views/cadastro/Produto.vue')
    }],
    meta: {
        requeresAuth: true
    }
}

let tipoProduto = {
  path: '/tipo-produto',
  component: () =>
      import ('@/views/cadastro/Tipo-Produto.vue'),
  children: [{
      path: '',
      component: () =>
          import ('@/views/cadastro/Tipo-Produto.vue')
  }],
  meta: {
      requeresAuth: true
  }
}

let posto = {
    path: '/posto',
    component: () =>
        import ('@/views/cadastro/Posto.vue'),
    children: [{
        path: '',
        component: () =>
            import ('@/views/cadastro/Posto.vue')
    }],
    meta: {
        requeresAuth: true
    }
}

Vue.use(Router)

export default new Router({
    routes: [{
        path: '/',
        component: DefaultLayout,
        meta: {
            showLeftDrawer: true // Para habilidade ou desabilitar o menu lateral esquerdo. Opcional, padrão 'true'.
        },
        children: [ // onde as páginas do projeto devem ser configuradas
            {
                path: '/',
                name: 'login',
                component: login
            },
            home,
            posto,
            registrar,
            companhia,
            produto,
            tipoProduto
        ]
    }]
})